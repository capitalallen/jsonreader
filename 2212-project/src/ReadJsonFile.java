
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
public class ReadJsonFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		ArrayList<String> buildings = new ArrayList<String>();
		ArrayList<String> floors = new ArrayList<String>();
		ArrayList<String> rooms = new ArrayList<String>();
		try {
			Object obj = parser.parse(new FileReader("src/buildings.json"));
			JSONObject jsonObj = (JSONObject) obj;
			for(Iterator iterator = jsonObj.keySet().iterator(); iterator.hasNext();) {
			    String key = (String) iterator.next();
			    buildings.add(key);
			    System.out.println(jsonObj.get(key));
			}
			for (int i=0;i<buildings.size();i++) {
				JSONObject floorNum = (JSONObject) jsonObj.get(buildings.get(i));
				System.out.println(buildings.get(i));
				for(Iterator iterator = floorNum.keySet().iterator(); iterator.hasNext();) {
				    String f = (String) iterator.next();
				    floors.add((f));
				    System.out.println(f);
				    System.out.println(floorNum.get(f));
				}
			}
			System.out.println(buildings);
			// print all buildings

			// print all floors 

			// print all rooms 
//			JSONObject studentDetails = (JSONObject)jsonObj.get("Buildings1");
//			System.out.println("studentDetails :"+studentDetails.toJSONString());
			
//			String studentName = (String)studentDetails.get("name");
//			System.out.println("StudentName :"+studentName);
//			
//			JSONArray subjects = (JSONArray)studentDetails.get("subjects");
//			System.out.println("Subject List :"+subjects);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
